package br.com.eduardo.pzzariadonmortadelo.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza  implements Parcelable {
    private String name;
    private String description;
    private float valueM;
    private float valueP;
    private float valueG;


    public Pizza(String name, String description, float valueM, float valueP, float valueG) {
        this.name = name;
        this.description = description;
        this.valueM = valueM;
        this.valueP = valueP;
        this.valueG = valueG;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getValueM() {
        return valueM;
    }

    public float getValueP() {
        return valueP;
    }

    public float getValueG() {
        return valueG;
    }

    protected Pizza(Parcel in) {
        name = in.readString();
        description = in.readString();
        valueM = in.readFloat();
        valueP= in.readFloat();
        valueG= in.readFloat();
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeFloat(valueM);
        dest.writeFloat(valueP);
        dest.writeFloat(valueG);
    }
}
