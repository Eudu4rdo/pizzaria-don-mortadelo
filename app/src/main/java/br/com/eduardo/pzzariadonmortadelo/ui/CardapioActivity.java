package br.com.eduardo.pzzariadonmortadelo.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.eduardo.pzzariadonmortadelo.R;
import br.com.eduardo.pzzariadonmortadelo.model.Pizza;

public class CardapioActivity extends AppCompatActivity {
    public static final String PIZZAS_LIST_KEY = "ListPizzaActivity.PIZZA_LIST";
    private LinearLayout lnv_pizzas;
    private ArrayList<Pizza> pizzas;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        this.pizzas = getIntent().getParcelableArrayListExtra(PIZZAS_LIST_KEY);
        this.lnv_pizzas = findViewById(R.id.lnv_pizzas);
        this.buildPizzaList();
    }

    private void buildPizzaList(){
        for(Pizza pizza: this.pizzas) {
            View PizzaItemView = getLayoutInflater().inflate(R.layout.pizza_item_view,
                    this.lnv_pizzas, false);
            TextView txtViewPizzaName = PizzaItemView.findViewById(R.id.txtViewPizzaName);
            TextView txtViewMedPrice = PizzaItemView.findViewById(R.id.txtViewMedPrice);
            TextView txtViewPizzaDescription = PizzaItemView.findViewById(R.id.txtViewPizzaDescription);
            txtViewPizzaName.setText(pizza.getName());
            txtViewPizzaDescription.setText(pizza.getDescription());
            txtViewMedPrice.setText("R$ "+pizza.getValueM());
            PizzaItemView.setTag(pizza);
            PizzaItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Pizza p =(Pizza) PizzaItemView.getTag();
                    onClickPizzaDetail(p);
                }
            });
            this.lnv_pizzas.addView(PizzaItemView);
        }
    }

    private void onClickPizzaDetail(Pizza pizza)
    {
        View dialogBody = getLayoutInflater().inflate(R.layout.pizza_info_dialog, null);
        TextView txtName= dialogBody.findViewById(R.id.txtPizzaName);
        TextView txtDescription= dialogBody.findViewById(R.id.txtDescription);
        TextView txtPriceP= dialogBody.findViewById(R.id.txtPriceP);
        TextView txtPriceM= dialogBody.findViewById(R.id.txtPriceM);
        TextView txtPriceG= dialogBody.findViewById(R.id.txtPriceG);

        txtName.setText(pizza.getName());
        txtDescription.setText(pizza.getDescription());
        txtPriceM.setText("R$ "+ pizza.getValueM());
        txtPriceP.setText("R$ "+ pizza.getValueP());
        txtPriceG.setText("R$ "+ pizza.getValueG());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.pizza_info)
                .setView(dialogBody)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing...
            }
        }).create().show();
    }
}