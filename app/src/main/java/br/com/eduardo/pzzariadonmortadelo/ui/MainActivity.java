package br.com.eduardo.pzzariadonmortadelo.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import br.com.eduardo.pzzariadonmortadelo.R;
import br.com.eduardo.pzzariadonmortadelo.model.Pizza;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_NEW_PIZZA_CODE = 444;
    public static final String SAVED_INSTANCE_KEY = "MainActivity.INSTANCE_LIST";
    private ArrayList <Pizza> pizzas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState != null && savedInstanceState.containsKey(SAVED_INSTANCE_KEY)){
            this.pizzas = savedInstanceState.getParcelableArrayList(SAVED_INSTANCE_KEY);
        }else {
            this.pizzas = new ArrayList<>();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SAVED_INSTANCE_KEY,this.pizzas);
    }

    public void onClickNewPizza(View view)
    {
        Intent newPizzaIntent = new Intent(this, NewPizzaActivity.class);
        startActivityForResult(newPizzaIntent, REQUEST_NEW_PIZZA_CODE);
    }

    public void onClickCardapio(View view)
    {
        Intent CardapioIntent = new Intent(this, CardapioActivity.class);
        CardapioIntent.putExtra(CardapioActivity.PIZZAS_LIST_KEY, this.pizzas);
        startActivity(CardapioIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== REQUEST_NEW_PIZZA_CODE && resultCode == RESULT_OK && data != null)
        {
                Pizza pizzaObj = data.getParcelableExtra(NewPizzaActivity.RESULT_KEY);
                this.pizzas.add(pizzaObj);
        }
    }
}