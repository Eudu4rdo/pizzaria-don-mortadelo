package br.com.eduardo.pzzariadonmortadelo.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import br.com.eduardo.pzzariadonmortadelo.R;
import br.com.eduardo.pzzariadonmortadelo.model.Pizza;

public class NewPizzaActivity extends AppCompatActivity {
    public static final String RESULT_KEY = "NewPizzaActivity.RESULT_PIZZA";
    private EditText etxName;
    private EditText etxValueM;
    private EditText etxDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pizza);
        this.etxName= findViewById(R.id.edtext_name);
        this.etxDescription= findViewById(R.id.edtext_description);
        this.etxValueM= findViewById(R.id.ednum_price);
    }
    public void onClickSave(View view)
    {
        String name= this.etxName.getText().toString();
        String description= this.etxDescription.getText().toString();
        Float valueM= Float.parseFloat(this.etxValueM.getText().toString());

        if(name.isEmpty()|| description.isEmpty() || valueM == null)
        {
            return;
        }
        else {
            float valueP = (float) (valueM * 0.25);
            float valueG = (float) (valueM * 1.25);
            Pizza pizza = new Pizza(name, description, valueM, valueP, valueG);
            Intent output = new Intent();
            output.putExtra(RESULT_KEY, pizza);
            setResult(RESULT_OK, output);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}